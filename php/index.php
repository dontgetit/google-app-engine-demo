<!DOCTYPE html>
<html>
<head>
  <title>App Engine Demo</title>
  <link rel="icon" type="image/x-icon" href="/images/favicon.ico">
</head>
<body>
<?php
switch (@parse_url($_SERVER['REQUEST_URI'])['path']) {
    case '/':
        require 'main.php';
        break;
    case '/about.php':
        require 'about.php';
        break;
    default:
        http_response_code(404);
	exit('Not Found');
}	
?>
</body>
</html>
