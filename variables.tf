variable "project_id" {
  type        = string
  description = "The id of GCP project to use."
}