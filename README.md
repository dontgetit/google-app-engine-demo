# gcp-app-engine-demo

Demo to deploy a very simple PHP application on Google Cloud Platform App Engine Standard application.

This GitLab project is an all in one containing PHP application code, Terraform and a GitLab CI/CD pipeline definition.

The configuration is hardcoded to use the europe-west2 GCP region.

*Project Overview*
![Overview of components in this project](/images/pipeline.jpg "Overview of Project")

## Getting started

Firstly, clone this repository.

Then, some boostrap actions need to be completed which are not automated but are needed in order to allow GitLab to work.

The following commands will do the bulk of the bootstrapping to create: 
- A new project 
- A Cloud Storage bucket for Terraform  state files 
- A service account, with key, to be used with GitLab CI/CD
- IAM permissions on the new service account
```
export $PROJECT_ID=memorable-and-unique 
gcloud project create $PROJECT_ID --name "Something memorable and unique" 
gcloud config set project $PROJECT_ID

gsutil mb -l EUROPE-WEST2 gs://${PROJECT_ID}-tfstate
gsutil versioning set on gs://${PROJECT_ID}-tfstate/
gcloud iam service-account create terraform
gcloud projects add-iam-policy-binding $PROJECT_ID --member="serviceAccount:terraform@$PROJECT_ID.iam.gserviceaccount.com" --role=roles/storage.admin
gcloud  projects add-iam-policy-binding $PROJECT_ID --member="serviceAccount:terraform@$PROJECT_ID.iam.gserviceaccount.com" --role=roles/appengine.appAdmin
gcloud  projects add-iam-policy-binding $PROJECT_ID --member="serviceAccount:terraform@$PROJECT_ID.iam.gserviceaccount.com" --role=roles/appengine.appCreator
gcloud iam service-accounts add-iam-policy-binding $PROJECT_ID@appspot.gserviceaccount.com --member="serviceAccount:terraform@$PROJECT_ID.iam.gserviceaccount.com" --role=roles/iam.serviceAccountUser
gcloud iam service-accounts keys create key.json --iam-account=terraform@$PROJECT_ID.iam.gserviceaccount.com
```
Also run the following to encode the service account key file
```
tr -d '\n' < key.json|base64 -w 0 > key.base64
```
And you will need to enable both the _App Engine API_ and _Cloud Build API_ in the new project. 

Finally also enable billing for the project.  (See the note below regarding costs.)

## GitLab CI/CD pipelines
#### Pipeline config

Two variables need to be defined: 
- GCP_SERVICE_ACCOUNT should be defined and should contain the contents of the key.base64 file, created above.
- GCP_PROJECT_ID should contain your GCP project ID.

#### Pipeline details

The pipeline contains stages to:
- Validate the included Terraform code 
- Perform simple syntax checking of the PHP code  
- Package (i.e. zip) the PHP application code 
- Plan Terraform changes
- And finally apply Terraform changes (to be release manually)

On first run of the pipeline Terraform will create App Engine components and get the application up and runnning.    The apply stage needs to be released manually.

Any subsequent commits to the project, for example changes to the PHP code, will trigger the pipeline to redeploy the application.

## Check if the deploy works
On successful completion of the GitLab pipeline, with manual release of the deploy stage, the app should be accessible at https://_GCP project id_.ew.r.appspot.com

## To modify the app
Change the files in the php directory  and commit to GitLab. The GitLab CI/CD pipeline will deploy the changes.

## Costs
This small demo should run within the limits of the GCP free tier and hopefully cost very little or nothing.
When finished with the demo remember to _shutdown the entire GCP project_ to avoid any unforseen spending on GCP.
