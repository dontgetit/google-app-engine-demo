resource "google_app_engine_application" "test-app-1" {
  location_id = "europe-west"

}

resource "google_app_engine_standard_app_version" "myapp_v1" {
  version_id = "v1"
  service    = "default"
  runtime    = "php74"

  entrypoint {
    shell = "serve index.php"
  }
  deployment {
    zip {
      source_url = "https://storage.googleapis.com/${google_storage_bucket.bucket.name}/${google_storage_bucket_object.object.name}"
    }
  }

  env_variables = {
    port = "8080"
  }

  handlers {
    url_regex = "/images/(.*)"
    static_files {
      path = "images/\\1"
      upload_path_regex = "images/.*"
    }
  } 

  automatic_scaling {
    max_concurrent_requests = 10
    min_idle_instances      = 1
    max_idle_instances      = 1
    min_pending_latency     = "1s"
    max_pending_latency     = "5s"
    standard_scheduler_settings {
      target_cpu_utilization        = 0.5
      target_throughput_utilization = 0.75
      min_instances                 = 1
      max_instances                 = 1
    }
  }
}

resource "google_storage_bucket" "bucket" {
  name     = "app-engine-deploy-${local.deploy_sha1}"
  location = "EUROPE-WEST2"
}

resource "google_storage_bucket_object" "object" {
  name   = "deployment.zip"
  bucket = google_storage_bucket.bucket.name
  source = "./deployment.zip"
}

locals {
  deploy_sha1 = sha1(join("", [for f in fileset(path.module, "php/*") : filesha1(f)]))
}
