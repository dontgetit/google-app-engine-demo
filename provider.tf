provider "google" {
  region      = "europe-west2"
  project     = var.project_id
  credentials = "./creds/serviceaccount.json"
}